#ifndef __defines_h__
#define __defines_h__


#define MCU_PWM_RESOLUTION (1 << 13);

#define MASTER_COMM_FREQ 20

#define POS_CONTROL_FREQ 100
#define POS_CONTROL_VAL_COUNT (int)( POS_CONTROL_FREQ / MASTER_COMM_FREQ )

#define SEND_TO_MONITOR_ON 1

#endif

