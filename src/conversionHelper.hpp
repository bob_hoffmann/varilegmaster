#include "data_interface_slave.h"
#include "defines.h"
#include "../../varileginterface/Sensor_Datenpacket.h"
#include "../../varileginterface/Commands_Datenpacket.h"

//constant factors depending on resolution setting etc.
#define IMU_GYRO_RES 1	//8.73/32768.0	//rad per second 	(500/360) * 2 PI
#define IMU_ACC_RES 1	//8.0/32768.0		//meter per second squared

#define GRAVITY_CONST 1 //9.81f

#define TWO_PI 6.282f


//sensor resolutions
#define SOFT_POT_RESOLUTION (1 << 12)	//this is the general resolution of a soft-/hippot

#define HIP_POT_RESOLUTION				6f / 1723f		//hip-pot left and right resolution
#define SOFT_POT_KNEE_RESOLUTION		4f / 826f		//soft-pot knee resolution
#define SOFT_POT_LEVERARM_RESOLUTION	4f / 1196f		//soft-pot lever-arm resolution
#define SOFT_POT_WINCH_RESOLUTION		706f			//soft-pot winch resolution



#define LeftHipPot2Rad(x) ((3.142/3.)/(2897.-1174.)*(x-1174.))
#define LeftWinchPot2Rad(x) (0.02/(2920.-2276.)*(x-2276.)/0.03)
#define LeftLeverPot2Rad(x) (3.142/2./(2726.-1675.)*(x-1675.))
#define LeftKneePot2Rad(x) (3.142/2./(2567.-1649.)*(x-1649.))

#define RightHipPot2Rad(x) ((3.142/3.)/(2772.-964.)*(x-964.))
#define RightWinchPot2Rad(x) (0.02/(2485.-1864.)*(x-1864.)/0.03)
#define RightLeverPot2Rad(x) (3.142/2./(2847.-1758.)*(x-1758.))
#define RightKneePot2Rad(x) (3.142/2./(2610.-1682.)*(x-1682.))




//offsets
#define IMU0_AX_OFFSET 0.0
#define IMU0_AY_OFFSET 0.0
#define IMU0_AZ_OFFSET 0.0

#define IMU1_AX_OFFSET 0.0
#define IMU1_AY_OFFSET 0.0
#define IMU1_AZ_OFFSET 0.0

#define IMU2_AX_OFFSET 0.0
#define IMU2_AY_OFFSET 0.0
#define IMU2_AZ_OFFSET 0.0

#define IMU3_AX_OFFSET 0.0
#define IMU3_AY_OFFSET 0.0
#define IMU3_AZ_OFFSET 0.0

#define IMU4_AX_OFFSET 0.0
#define IMU4_AY_OFFSET 0.0
#define IMU4_AZ_OFFSET 0.0

#define IMU5_AX_OFFSET 0.0
#define IMU5_AY_OFFSET 0.0
#define IMU5_AZ_OFFSET 0.0

#define IMU6_AX_OFFSET 0.0
#define IMU6_AY_OFFSET 0.0
#define IMU6_AZ_OFFSET 0.0


#define SOFT_POT0_OFFSET -1410f
#define SOFT_POT1_OFFSET -1640f
#define SOFT_POT2_OFFSET -2478f
#define SOFT_POT3_OFFSET 0.0
#define SOFT_POT4_OFFSET 0.0
#define SOFT_POT5_OFFSET 0.0

#define HIP_POT0_OFFSET -1174f
#define HIP_POT1_OFFSET 0.0


//identifiers
enum TD_identifier{
	TD_IMU0,	
	TD_IMU1,	
	TD_IMU2,	
	TD_IMU3,	
	TD_IMU4,	
	TD_IMU5,	
	TD_IMU6,	

	TD_SOFT_POT0,	//left knee
	TD_SOFT_POT1,	//left lever arm
	TD_SOFT_POT2,	//left roll
	TD_SOFT_POT3,	//right roll
	TD_SOFT_POT4,	//right lever arm
	TD_SOFT_POT5,	//right knee

	TD_HIP_POT0,	//left hip pot
	TD_HIP_POT1,	//right hip pot

	TD_LOAD_CELL0,	//left foot
	TD_LOAD_CELL1,	//left foot
	TD_LOAD_CELL2,	//left foot
	TD_LOAD_CELL3,	//left foot
	TD_LOAD_CELL4,	//right foot
	TD_LOAD_CELL5,	//right foot
	TD_LOAD_CELL6,	//right foot
	TD_LOAD_CELL7,	//right foot
};

//identifiers for commands
enum MA_identifier{
	SLAVE1 = 1,
	SLAVE2,
	SLAVE3,
};

void transfer_commands(Master_Data *master_data, Commands_Datenpacket *commands_data, int identifier){
 


	switch(identifier)
	{
		case SLAVE1:
			//Process Slave 1
			for(int i =0;i<POS_CONTROL_VAL_COUNT;i++){
				master_data->MCU1_POS[i]= (uint16_t)commands_data->motorposition[3][i];
				master_data->MCU2_POS[i]= (uint16_t)commands_data->motorposition[4][i];
			}
			master_data->COUNTER++;
			master_data->FLAGS=0;
			master_data->MCU1_PID_P=0;
			master_data->MCU1_PID_I=0;
			master_data->MCU1_PID_D=0;
			master_data->MCU2_PID_P=0;
			master_data->MCU2_PID_I=0;
			master_data->MCU2_PID_D=0;		
			break;


		case SLAVE2:
			//Process Slave 2
			for(int i =0;i<POS_CONTROL_VAL_COUNT;i++){
				master_data->MCU1_POS[i]= (uint16_t)commands_data->motorposition[5][i];
				master_data->MCU2_POS[i]= (uint16_t)commands_data->motorposition[2][i];
			}
			master_data->COUNTER++;
			master_data->FLAGS=0;
			master_data->MCU1_PID_P=0;
			master_data->MCU1_PID_I=0;
			master_data->MCU1_PID_D=0;
			master_data->MCU2_PID_P=0;
			master_data->MCU2_PID_I=0;
			master_data->MCU2_PID_D=0;
			break;


		case SLAVE3:
			//Process Slave 3
			for(int i =0;i<POS_CONTROL_VAL_COUNT;i++){
				master_data->MCU1_POS[i]= (uint16_t)commands_data->motorposition[0][i];
				master_data->MCU2_POS[i]= (uint16_t)commands_data->motorposition[1][i];
			}
			master_data->COUNTER++;
			master_data->FLAGS=0;
			master_data->MCU1_PID_P=0;
			master_data->MCU1_PID_I=0;
			master_data->MCU1_PID_D=0;
			master_data->MCU2_PID_P=0;
			master_data->MCU2_PID_I=0;
			master_data->MCU2_PID_D=0;
			break;


	


	}

}


void transfer_data(Slave_Data *slave_data, Sensor_Datenpacket *sensor_data, int identifier)
{
	switch(identifier)
	{
		case TD_IMU1:
			//processs IMU1
			sensor_data->IMU[1][0] = (float)slave_data->IMU2_GX * IMU_GYRO_RES;
			sensor_data->IMU[1][1] = (float)slave_data->IMU2_GY * IMU_GYRO_RES;
			sensor_data->IMU[1][2] = (float)slave_data->IMU2_GZ * IMU_GYRO_RES;
			sensor_data->IMU[1][3] = (float)slave_data->IMU2_AX * IMU_ACC_RES * GRAVITY_CONST + IMU1_AX_OFFSET;
			sensor_data->IMU[1][4] = (float)slave_data->IMU2_AY * IMU_ACC_RES * GRAVITY_CONST + IMU1_AY_OFFSET;
			sensor_data->IMU[1][5] = (float)slave_data->IMU2_AZ * IMU_ACC_RES * GRAVITY_CONST + IMU1_AZ_OFFSET;
			break;
		case TD_IMU2:
			//processs IMU2
			sensor_data->IMU[2][0] = (float)slave_data->IMU3_GX * IMU_GYRO_RES;
			sensor_data->IMU[2][1] = (float)slave_data->IMU3_GY * IMU_GYRO_RES;
			sensor_data->IMU[2][2] = (float)slave_data->IMU3_GZ * IMU_GYRO_RES;
			sensor_data->IMU[2][3] = (float)slave_data->IMU3_AX * IMU_ACC_RES * GRAVITY_CONST + IMU2_AX_OFFSET;
			sensor_data->IMU[2][4] = (float)slave_data->IMU3_AY * IMU_ACC_RES * GRAVITY_CONST + IMU2_AY_OFFSET;
			sensor_data->IMU[2][5] = (float)slave_data->IMU3_AZ * IMU_ACC_RES * GRAVITY_CONST + IMU2_AZ_OFFSET;
			break;
		case TD_IMU3:
			//processs IMU3
			sensor_data->IMU[3][0] = (float)slave_data->IMU1_GX * IMU_GYRO_RES;
			sensor_data->IMU[3][1] = (float)slave_data->IMU1_GY * IMU_GYRO_RES;
			sensor_data->IMU[3][2] = (float)slave_data->IMU1_GZ * IMU_GYRO_RES;
			sensor_data->IMU[3][3] = (float)slave_data->IMU1_AX * IMU_ACC_RES * GRAVITY_CONST + IMU3_AX_OFFSET;
			sensor_data->IMU[3][4] = (float)slave_data->IMU1_AY * IMU_ACC_RES * GRAVITY_CONST + IMU3_AY_OFFSET;
			sensor_data->IMU[3][5] = (float)slave_data->IMU1_AZ * IMU_ACC_RES * GRAVITY_CONST + IMU3_AZ_OFFSET;
			break;
		case TD_IMU4:
			//processs IMU4
			sensor_data->IMU[4][0] = (float)slave_data->IMU3_GX * IMU_GYRO_RES;
			sensor_data->IMU[4][1] = (float)slave_data->IMU3_GY * IMU_GYRO_RES;
			sensor_data->IMU[4][2] = (float)slave_data->IMU3_GZ * IMU_GYRO_RES;
			sensor_data->IMU[4][3] = (float)slave_data->IMU3_AX * IMU_ACC_RES * GRAVITY_CONST + IMU4_AX_OFFSET;
			sensor_data->IMU[4][4] = (float)slave_data->IMU3_AY * IMU_ACC_RES * GRAVITY_CONST + IMU4_AY_OFFSET;
			sensor_data->IMU[4][5] = (float)slave_data->IMU3_AZ * IMU_ACC_RES * GRAVITY_CONST + IMU4_AZ_OFFSET;
			break;
		case TD_IMU5:
			//processs IMU5
			sensor_data->IMU[5][0] = (float)slave_data->IMU2_GX * IMU_GYRO_RES;
			sensor_data->IMU[5][1] = (float)slave_data->IMU2_GY * IMU_GYRO_RES;
			sensor_data->IMU[5][2] = (float)slave_data->IMU2_GZ * IMU_GYRO_RES;
			sensor_data->IMU[5][3] = (float)slave_data->IMU2_AX * IMU_ACC_RES * GRAVITY_CONST + IMU5_AX_OFFSET;
			sensor_data->IMU[5][4] = (float)slave_data->IMU2_AY * IMU_ACC_RES * GRAVITY_CONST + IMU5_AY_OFFSET;
			sensor_data->IMU[5][5] = (float)slave_data->IMU2_AZ * IMU_ACC_RES * GRAVITY_CONST + IMU5_AZ_OFFSET;
			break;
		case TD_SOFT_POT0:	//left knee
			sensor_data->HOTPOT[0] =(float) LeftKneePot2Rad(slave_data->SOFT_POT_1);
			break;
		case TD_SOFT_POT1:	//left lever
			sensor_data->HOTPOT[1] =(float) LeftLeverPot2Rad(slave_data->SOFT_POT_2);
			break;
		case TD_SOFT_POT2:	//left winch
			sensor_data->HOTPOT[2] =(float) LeftWinchPot2Rad(slave_data->SOFT_POT_3);
			break;
		case TD_SOFT_POT3:	//right knee
			sensor_data->HOTPOT[3] =(float) RightKneePot2Rad(slave_data->SOFT_POT_1);
			break;
		case TD_SOFT_POT4:	//right lever
			sensor_data->HOTPOT[4] =(float) RightLeverPot2Rad(slave_data->SOFT_POT_2);
			break;	
		case TD_SOFT_POT5:	//right winch
			sensor_data->HOTPOT[5] =(float) RightWinchPot2Rad(slave_data->SOFT_POT_3);
			break;
		case TD_HIP_POT0:	//left hip
			sensor_data->HOTPOT[6] =(float) LeftHipPot2Rad(slave_data->HIP_POT_1);
			break;
		case TD_HIP_POT1:	//right hip
			sensor_data->HOTPOT[7] =(float) RightHipPot2Rad(slave_data->HIP_POT_2);
			break;

	}

}
