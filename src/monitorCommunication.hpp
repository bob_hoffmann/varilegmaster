#ifndef MONITORCOMM_H
#define MONITORCOMM_H

#include <zmq.hpp>

//tries to send data to slave and returns true if success
bool zmq_send(std::vector<unsigned char> msg, zmq::socket_t *socket)
{
	zmq::message_t _msg(msg.size());
	memcpy((void *) _msg.data(), msg.data(), msg.size());
	socket->send(_msg, ZMQ_NOBLOCK);
	return true;
}

#endif
